"use strict"

// потому что текст может быть вставлен с помощью мыши

const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', e => {
    buttons.forEach(item => {
        if (e.code === item.dataset.key) {
            item.classList.add('active');
        } else {
            item.classList.remove('active');
        }
    })
})